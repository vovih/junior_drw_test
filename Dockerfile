# python:3.8 -> python:3.8-alpine
FROM python:3.8-alpine 
COPY requirements/production.txt /tmp/requirements/production.txt
RUN pip install --no-cache -r  /tmp/requirements/production.txt

#
# public -> dist
COPY public /tmp/dist 
# pip install /tmp/dist/*
RUN pip install --no-cache /tmp/dist/* 

COPY run_service.py run_servicer.py

EXPOSE 8085

CMD ["python", "run_servicer.py", "--port", "8085"]
